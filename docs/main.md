gqlite documentation
====================

- [Installation](installation.md) instructions
- [API Documentation](api.md) for the different supported programming language
- [Open Cypher Query Language](opencypher.md) documentation of the implementation